module.exports = {
  publicPath: process.env.VUE_APP_BASE_PATH || '/',
  transpileDependencies: ['vuetify'],
  lintOnSave: false,
  css: {
    loaderOptions: {
      less: {
        globalVars: {
          blue: '#187CC6',
          lightBlue: '#9BD0FF',
        },
      },
    },
  },
};
