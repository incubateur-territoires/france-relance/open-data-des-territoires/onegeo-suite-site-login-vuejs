# login-site

## Project setup

```shell
npm install
```

### Set environment variables

```ìni
NODE_ENV=development
VUE_APP_LOCALE=fr-FR

DOMAIN=http://127.0.0.0:80
VUE_APP_DOMAIN=${DOMAIN}

BASE_PATH=/
VUE_APP_BASE_PATH=${BASE_PATH}

VUE_APP_NEXT_DEFAULT=${BASE_PATH}

# App title
VUE_APP_TITLE=Onegeo-Suite

# Client name
VUE_APP_CLIENT_NAME=ONEGEOSUITE

# Client contact mail
VUE_APP_CLIENT_CONTACT_MAIL=admin@contact.org

# Favicon
VUE_APP_FAVICON_URL=${VUE_APP_DOMAIN}/assets/favicon.png

#Logo
VUE_APP_LOGO=@/assets/logo.png

# API
VUE_APP_LOGIN_API_PATH=/fr/login
VUE_APP_ORGANISATION_API_PATH=/fr/organisation/
VUE_APP_USERGROUP_API_PATH=/fr/usergroup/

# AUTH
VUE_APP_LOGIN_API_USERNAME=admin
VUE_APP_LOGIN_API_PASSWORD=CHANGE_ME
```

### Compiles and hot-reloads for development

```shell
npm run serve
```

### Compiles and minifies for production

```shell
npm run build
```

### Run your unit tests

```shell
npm run test:unit
```

### Run your end-to-end tests

```shell
npm run test:e2e
```

### Lints and fixes files

```shell
npm run lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
