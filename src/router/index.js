import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    redirect: { name: 'SignIn' },
  },
  {
    path: '/signin',
    name: 'SignIn',
    component: () => import('@/views/SignIn.vue'),
  },
  {
    path: '/signup',
    name: 'SignUp',
    component: () => import('@/views/SignUp.vue'),
  },
  {
    path: '/terms-of-use',
    name: 'TermsOfUse',
    component: () => import('@/views/TermsOfUse.vue'),
  },
  {
    path: '/signout',
    name: 'SignOut',
    component: () => import('@/views/SignOut.vue'),
  },
  {
    path: '/signout-failed',
    name: 'SignOutFailed',
    component: () => import('@/views/SignOutFailed.vue'),
  },
  {
    path: '/signupsuccess',
    name: 'SignUpSuccess',
    component: () => import('@/views/SignUpSuccess.vue'),
  },
  {
    path: '/validateregistration',
    name: 'ValidationRegistration',
    component: () => import('@/views/ValidationRegistration.vue'),
  },
  {
    path: '/validate-email',
    name: 'ValidationEmail',
    component: () => import('@/views/ValidationEmail.vue'),
  },
  {
    path: '/forgottenpwd',
    name: 'ForgottenPassword',
    component: () => import('@/views/ForgottenPassword.vue'),
  },
  {
    path: '/reinitpwd',
    name: 'ReinitPassword',
    component: () => import('@/views/ReinitPassword.vue'),
  },
  {
    path: '/profile',
    name: 'UserProfile',
    component: () => import('@/views/UserProfile.vue'),
  },
  {
    path: '/*',
    name: 'NotFound',
    component: () => import('@/views/NotFound.vue'),
  },
];

const router = new VueRouter({
  routes,
  mode: 'history',
  base: process.env.VUE_APP_BASE_PATH || '',
});

export default router;
