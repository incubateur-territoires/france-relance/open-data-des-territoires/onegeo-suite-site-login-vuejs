import Vue from 'vue';

import { BootstrapVue, BootstrapVueIcons } from 'bootstrap-vue';
import 'bootstrap-vue/dist/bootstrap-vue-icons.min.css';
import Multiselect from 'vue-multiselect';

// Custom css
import '@/app.scss';
import '@/app.less';

Vue.use(BootstrapVue);
Vue.use(BootstrapVueIcons);
Vue.component('Multiselect', Multiselect);

import App from '@/App.vue';
import router from '@/router';
import store from '@/store';

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app')
