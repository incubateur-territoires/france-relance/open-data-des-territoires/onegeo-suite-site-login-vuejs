import loginAPI from '@/api/loginAPI.js';

import { ErrorService } from '@/services/error-service.js';

// MUTATIONS
export const SET_ERROR = 'SET_ERROR';
export const SET_SUCCESS = 'SET_SUCCESS';
export const SET_USER_DETAIL = 'SET_USER_DETAIL';

// ACTIONS
export const GET_USER_DETAIL = 'GET_USER_DETAIL';
export const UPDATE_USER_DETAIL = 'UPDATE_USER_DETAIL';

/**************** STATE *******************/
const state = {
  userData: null,
  userError: null,
  error: null,
  success: null
};

/**************** GETTERS *****************/
const getters = {

};

/*************** MUTATIONS ****************/
const mutations = {
  [SET_USER_DETAIL]: (state, payload) => {
    state.userData = payload;
  },

  [SET_ERROR]: (state, error) => {
    if (error) {
      ErrorService.onError(error);
      state.userError = error.response.data.detail;
    } else {
      state.userError = error;
    }
  },

  [SET_SUCCESS]: (state, payload) => {
    state.error = null;
    state.success = payload.message;
  },
};
/**************** ACTIONS *****************/
const actions = {


  [GET_USER_DETAIL]: async ({ commit }) => {
    await loginAPI.getUserDetail()
    .then((resp) => {
      if (resp) {
        commit('SET_ERROR', null);
        commit('SET_USER_DETAIL', resp);
      }
    })
    .catch((error) => {
      commit('SET_ERROR', error);
    });
  },

  [UPDATE_USER_DETAIL]: async ({ commit }, data) => {
    await loginAPI.updateUserDetail(data)
    .then((resp) => {
      if (resp) {
        commit('SET_ERROR', null);
        commit('SET_SUCCESS', {
          response: resp,
          message: 'Votre mot de passe a bien été modifié.'
        });
      }
    })
    .catch((error) => {
      commit('SET_SUCCESS', {
        response: null,
        message: null
      });
      commit('SET_ERROR', error);
    });
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};