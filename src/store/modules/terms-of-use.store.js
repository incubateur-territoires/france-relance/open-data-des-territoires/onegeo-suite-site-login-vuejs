import client from '@/api/loginAPI.js';
import { ErrorService } from '@/services/error-service.js';
import router from '@/router';

const state = {
  terms: null,
  error: null,
  success: null
};

const getters = {
};

export const GET_TERMS_OF_USE = 'GET_TERMS_OF_USE';
export const AGREE_TO_TERMS_OF_USE = 'AGREE_TO_TERMS_OF_USE';

const actions = {
  [GET_TERMS_OF_USE]: async ({ commit }) => {
    const response = await client.getTermsOfUse();
    if (response.status === 200) {
      commit('SET_TERMS', response.data);
      commit('SET_SUCCESS', {
        response: response,
        message: ''
      });
    }
    if (response.status === 404) {
      commit('SET_ERROR', {
        response: response,
        message: 'Une erreur est survenue.'
      });
    }
  },
  [AGREE_TO_TERMS_OF_USE]: async ({ commit }, data) => {
    const response = await client.postTermsOfUseAgreement(data);
    if (response.status === 200) {
      commit('SET_SUCCESS', {
        response: response,
        message: ''
      });
      window.location.href = router.currentRoute.query.next;
    }
    if (response.status === 404) {
      commit('SET_ERROR', {
        response: response,
        message: 'Une erreur est survenue.'
      });
    }
  }
};

export const SET_TERMS = 'SET_TERMS';
export const SET_ERROR = 'SET_ERROR';
export const SET_SUCCESS = 'SET_SUCCESS';

const mutations = {
  [SET_TERMS]: (state, payload) => {
    state.terms = payload;
  },
  [SET_ERROR]: (state, payload) => {
    state.success = null;
    ErrorService.onError(payload.response);
    state.error = payload.message;
  },
  [SET_SUCCESS]: (state, payload) => {
    state.error = null;
    state.success = payload.message;
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
