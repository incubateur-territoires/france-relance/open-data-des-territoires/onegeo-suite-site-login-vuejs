import client from '@/api/loginAPI.js';
import { ErrorService } from '@/services/error-service.js';
import Swal from "sweetalert2";
import "sweetalert2/dist/sweetalert2.min.css";
import router from '@/router';

const state = {
  error: null,
  success: null
};

const getters = {
};

export const REQUEST_FORGOTTEN_PASSWORD = 'REQUEST_FORGOTTEN_PASSWORD';
export const CONFIRM_NEW_PASSWORD = 'CONFIRM_NEW_PASSWORD';

const actions = {
  [REQUEST_FORGOTTEN_PASSWORD]: async ({ commit }, data) => {
    const response = await client.forgottenPasswordRequest(data);
    if (response.status === 200) {
      Swal.fire({
        position: 'center',
        heightAuto: false,
        icon: 'success',
        text: `
          Un e-mail vous a été envoyé sur votre messagerie pour réinitialiser votre mot de passe.
        `,
        showConfirmButton: true,
        confirmButtonText: 'OK',
        confirmButtonColor: '#187CC6'
      }).then((result) => {
        if (result.isConfirmed) {
          router.push({ name: 'SignIn' });
        }
      });
    }
    if (response.status === 404) {
      commit('SET_ERROR', {
        response: response,
        message: 'Aucun compte trouvé pour cette adresse e-mail.'
      });
    }
  },

  [CONFIRM_NEW_PASSWORD]: async ({ commit }, data) => {
    const response = await client.forgottenPasswordConfirm(data);
    if (response.status === 200) {
      Swal.fire({
        position: 'center',
        heightAuto: false,
        icon: 'success',
        text: `Votre mot de passe a bien été réinitialisé.
          Vous allez être redirigé vers la page de connexion.
        `,
        showConfirmButton: true,
        confirmButtonText: 'OK',
        confirmButtonColor: '#187CC6'
      }).then((result) => {
        if (result.isConfirmed) {
          router.push({ name: 'SignIn' });
        }
      });
    } else if (response.status === 400) {
      Swal.fire({
        position: 'center',
        heightAuto: false,
        icon: 'error',
        text: `Votre token a expiré. Vous devez en demander un nouveau 
          pour changer votre mot de passe.
        `,
        showConfirmButton: true,
        confirmButtonText: 'OK',
        confirmButtonColor: '#187CC6'
      }).then((result) => {
        if (result.isConfirmed) {
          router.push({ name: 'SignIn' });
        }
      });
    }
  }
};

export const SET_ERROR = 'SET_ERROR';
export const SET_SUCCESS = 'SET_SUCCESS';

const mutations = {
  [SET_ERROR]: (state, payload) => {
    state.success = null;
    ErrorService.onError(payload.response);
    state.error = payload.message;
  },
  [SET_SUCCESS]: (state, payload) => {
    state.error = null;
    state.success = payload.message;
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
