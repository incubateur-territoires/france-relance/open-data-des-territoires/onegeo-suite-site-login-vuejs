import client from '@/api/loginAPI.js';

const state = {
  logged: null,
  error: null
};

const getters = {
};

export const GET_SIGNOUT = 'GET_SIGNOUT';

const actions = {
  [GET_SIGNOUT]: async ({ commit }) => {
    await client.signOut()
      .then(
        () => {
          commit('SET_ERROR', undefined);
          commit('SET_LOGGED', false);
        },
      )
      .catch(
        (error) => {
          commit(
            'SET_ERROR',
            error.response.data
            || 'Une erreur est survenue',
          );
          commit('SET_LOGGED', true);
        },
      );
  },
};

export const SET_ERROR = 'SET_ERROR';
export const SET_LOGGED = 'SET_LOGGED';

const mutations = {
  [SET_LOGGED]: (state, value) => {
    if (value === true) {
      state.logged = true;
    } else {
      state.logged = false;
    }
  },
  [SET_ERROR]: (state, payload) => {
    state.error = payload;
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
